﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CarMVCUploadServer.Models
{
    public class Car
    {
        public int Id { get; set; }

        public string CarName { get; set; }

        public string Model { get; set; }

        public int Price { get; set; }

        public string ImageUrl { get; set; }

        [NotMapped]
        public IFormFile file { get; set; }
        [NotMapped]
        public byte[] bytes { get; set; }
    }
}
