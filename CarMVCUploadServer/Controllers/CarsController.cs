﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CarMVCUploadServer.Data;
using CarMVCUploadServer.Models;
using System.IO;
using System.Net.Http.Headers;
using static System.Net.Mime.MediaTypeNames;

namespace CarMVCUploadServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly CarMVCUploadServerContext _context;
        public static readonly string IMG_DIR = Directory.GetCurrentDirectory();

        public CarsController(CarMVCUploadServerContext context)
        {
            _context = context;
        }

        // GET: api/Cars
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Car>>> GetCar()
        {
            return await _context.Car.ToListAsync();
        }

        // GET: api/Cars/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Car>> GetCar(int id)
        {
            var car = await _context.Car.FindAsync(id);

            if (car == null)
            {
                return NotFound();
            }

            return car;
        }

        // PUT: api/Cars/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCar(int id, Car car)
        {
            if (id != car.Id)
            {
                return BadRequest();
            }
            //Delete old Image
            if (System.IO.File.Exists(Path.Combine(IMG_DIR, @"wwwroot/", car.ImageUrl)))
            {
                System.IO.File.Delete(Path.Combine(IMG_DIR, @"wwwroot/", car.ImageUrl));
            }
            string filePath = Path.Combine(IMG_DIR, @"wwwroot/imgs", car.ImageUrl);
            car.ImageUrl = Path.Combine("imgs", car.ImageUrl);
            MemoryStream ms = new MemoryStream(car.bytes, 0, car.bytes.Length);
            using (FileStream file = new FileStream(filePath, FileMode.Create, System.IO.FileAccess.Write))
            {
                await ms.CopyToAsync(file);
            }

            _context.Entry(car).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Cars
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Car>> PostCar(Car car)
        {
            string filePath = Path.Combine(IMG_DIR, @"wwwroot/imgs", car.ImageUrl);
            car.ImageUrl = Path.Combine("imgs",car.ImageUrl);
            MemoryStream ms = new MemoryStream(car.bytes, 0, car.bytes.Length);
            using (FileStream file = new FileStream(filePath, FileMode.Create, System.IO.FileAccess.Write))
            {
                await ms.CopyToAsync(file);
            }
            
            _context.Car.Add(car);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetCar", new { id = car.Id }, car);
        }

        // DELETE: api/Cars/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Car>> DeleteCar(int id)
        {
            var car = await _context.Car.FindAsync(id);
            if (car == null)
            {
                return NotFound();
            }
            if(System.IO.File.Exists(Path.Combine(IMG_DIR,@"wwwroot/", car.ImageUrl))){
                System.IO.File.Delete(Path.Combine(IMG_DIR, @"wwwroot/", car.ImageUrl));
            }

            _context.Car.Remove(car);
            await _context.SaveChangesAsync();

            return car;
        }

        private bool CarExists(int id)
        {
            return _context.Car.Any(e => e.Id == id);
        }
    }
}
