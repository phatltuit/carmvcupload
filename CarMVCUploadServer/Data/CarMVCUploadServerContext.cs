﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CarMVCUploadServer.Models;

namespace CarMVCUploadServer.Data
{
    public class CarMVCUploadServerContext : DbContext
    {
        public CarMVCUploadServerContext (DbContextOptions<CarMVCUploadServerContext> options)
            : base(options)
        {
        }

        public DbSet<CarMVCUploadServer.Models.Car> Car { get; set; }
    }
}
