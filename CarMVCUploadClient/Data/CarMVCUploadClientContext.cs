﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CarMVCUploadClient.Models;

namespace CarMVCUploadClient.Data
{
    public class CarMVCUploadClientContext : DbContext
    {
        public CarMVCUploadClientContext (DbContextOptions<CarMVCUploadClientContext> options)
            : base(options)
        {
        }

        public DbSet<CarMVCUploadClient.Models.Car> Car { get; set; }
    }
}
