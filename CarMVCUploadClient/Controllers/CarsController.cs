﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CarMVCUploadClient.Data;
using CarMVCUploadClient.Models;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;

namespace CarMVCUploadClient.Controllers
{
    public class CarsController : Controller
    {
        private readonly CarMVCUploadClientContext _context;
        private readonly string api = "http://localhost:2615/api/Cars";
        private readonly HttpClient client = new HttpClient();
        private readonly string IMG_DIR = Directory.GetCurrentDirectory();
        public static string SERVER_URL = "http://localhost:2615/";



        public CarsController(CarMVCUploadClientContext context)
        {
            _context = context;
        }

        // GET: Cars
        public async Task<IActionResult> Index()
        {
            List<Car> cars = new List<Car>();

            using (var response = await client.GetAsync(api))
            {
                string apiResponse = await response.Content.ReadAsStringAsync();
                cars = JsonConvert.DeserializeObject<List<Car>>(apiResponse);
                if (response.IsSuccessStatusCode)
                {
                    return View(cars);
                }
            }
            return NotFound();
            
        }

        // GET: Cars/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            using (var response = await client.GetAsync(api+"/"+id))
            {
                Car car;
                string apiResponse = await response.Content.ReadAsStringAsync();                
                if (response.IsSuccessStatusCode)
                {
                    car = JsonConvert.DeserializeObject<Car>(apiResponse);
                    return View(car);
                }
            }
            return NotFound();
        }

        // GET: Cars/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Cars/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CarName,Model,Price,ImageUrl")] Car car, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                car.ImageUrl = Path.GetFileName(file.FileName);                
                if (file.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        await file.CopyToAsync(ms);
                        byte[] fileBytes = ms.ToArray();
                        string s = Convert.ToBase64String(fileBytes);
                        car.bytes = fileBytes;
                    }
                }
                using (var response = await client.PostAsync(api, new StringContent(JsonConvert.SerializeObject(car), Encoding.UTF8, "application/json")))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return NotFound();
                    }
                }
            }
            return NotFound();
        }

        // GET: Cars/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var car = await _context.Car.FindAsync(id);
            if (car == null)
            {
                return NotFound();
            }
            return View(car);
        }

        // POST: Cars/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CarName,Model,Price,ImageUrl")] Car car, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                car.ImageUrl = Path.GetFileName(file.FileName);
                if (file.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        await file.CopyToAsync(ms);
                        byte[] fileBytes = ms.ToArray();
                        string s = Convert.ToBase64String(fileBytes);
                        car.bytes = fileBytes;
                    }
                }

                using (var response = await client.PutAsync(api + "/" + id, new StringContent(JsonConvert.SerializeObject(car), Encoding.UTF8, "application/json")))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return NotFound();
                    }
                }      
            }
            return NotFound();
        }

        // GET: Cars/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var car = await _context.Car
                .FirstOrDefaultAsync(m => m.Id == id);
            if (car == null)
            {
                return NotFound();
            }

            return View(car);
        }

        // POST: Cars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            using (var response = await client.DeleteAsync(api + "/" + id))
            {
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            return NotFound();
        }

        private bool CarExists(int id)
        {
            return _context.Car.Any(e => e.Id == id);
        }
    }
}
